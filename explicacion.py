
pila = Stack()

pila.push(1)

# 1
print(pila.top()) # 1
print(pila.size) # 1


pila.push(2) 

print(pila.top()) # 2
print(pila.size) # 2
# 1 2

pila.push(1) 

print(pila.top()) # 1
print(pila.size) # 3


pila.pop()

# 1

pila.push(2)
pila.push(3)

# 1 2 3

pila.push(1)

# 1 2 3 1

pila.pop()

# 1 2 3


























class Nodo:
    # Acá va todo el código para la entidad Nodo
    pass

class Lista_enlazada:
    # Acá va todo el código para la lista enlazada
    def agregar(self,valor):

        unNodo = Nodo(valor)
        # Empezamos a recorrer la lista hasta llegar al final
        #   y ahí agregamos el nodo
        print(unNodo.valor)


class Album:
    def __init__(self):
        self.lista_de_figuritas = # Acá es donde esa lista empieza vacía...
        self.repetidas = #....

    def pegarUnaFigurita(self,figurita):
        if self.lista_de_figuritas.buscar(figurita):
            # ......
        else:
            self.lista_de_figuritas.agregar(figurita)

    def estaRepetida(self,figurita):
        self.lista_de_figuritas.buscar(figurita)

    def pegarUnSobreDeFiguritas(self,unSobre):
        pass
    
    def pegarFiguritasDeListaDeRepetidas(self,unaListaDeRepetidas):
        # Iterar la lista de repetidas
        # para cada una de esas repetidas, ver si la pego o no en
        #   mi álbum

unSobre = [1,2,3,4,5]

unAlbum = Album()

unAlbum.pegarUnaFigurita(unSobre[0])
unAlbum.pegarUnaFigurita(unSobre[1])
unAlbum.pegarUnaFigurita(unSobre[2])
unAlbum.pegarUnaFigurita(unSobre[3])
unAlbum.pegarUnaFigurita(unSobre[4])

unAlbum.pegarUnSobreDeFiguritas(unSobre)